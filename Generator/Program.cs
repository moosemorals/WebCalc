using System;
using System.IO;
using System.Linq;
using System.Text;

namespace Uk.Osric.Caculator.Generator;

public class Program
{

    public static void Main(string[] args)
    {

        string outputFolder = @"C:\Users\Osric Wilkinson\source\repos\WebCalc\WebCalc\wwwroot";

        DefineAst(outputFolder, "Expr",
            "Assign     : name, value       : Token, Expr",
            "Binary     : left, op, right   : Expr, Token, Expr",
            "Grouping   : expression        : Expr",
            "Literal    : value             : ?any",
            "Unary      : op, right         : Token, Expr",
            "Variable   : name              : Token "
            );

        DefineAst(outputFolder, "Stmt",
            "Block      : statements        : Stmt[]",
            "Expression : expression        : Expr",
            "If         : condition, thenBranch, elseBranch : Expr, Stmt, Stmt?",
            "Print      : expression        : Expr",
            "Var        : name, initializer : Token, ?Expr");
    }

    private static void DefineAst(
        string outputFolder,
        string baseName,
        params string[] typeNames)
    {

        string path = Path.Join(outputFolder, baseName.ToLower() + ".js");
        StringBuilder result = new();

        result.AppendLine(@"/* SPDX-License-Identifier: ISC */");

        result.AppendLine($"/* File generated at {DateTimeOffset.Now.ToString("o")} by {typeof(Program).FullName} */");

        foreach (string type in typeNames)
        {
            string className = type.Split(':')[0].Trim();
            string fields = type.Split(':')[1].Trim();
            string types = type.Split(':')[2].Trim();
            DefineType(result, baseName, className, fields, types);
        }

        File.WriteAllText(path, result.ToString());
    }

    private static void DefineType(
        StringBuilder result,
        string baseName,
        string className,
        string fieldString,
        string typeString
        )
    {
        string[] types = typeString.Split(',', System.StringSplitOptions.TrimEntries);
        string[] fields = fieldString.Split(',', System.StringSplitOptions.TrimEntries);

        // Class definition
        result.AppendLine($"export class {className} {{");

        // JSDoc for constructor
        result.AppendLine("    /**");
        foreach ((string t, string f) in types.Zip(fields))
        {
            result.AppendLine($"    * @param {{{t}}} {f}");
        }
        result.AppendLine("    */");

        // Constructor
        result.AppendLine($"    constructor({fieldString}) {{");
        foreach (string f in fields)
        {
            result.AppendLine($"        this.{f} = {f};");
        }
        result.AppendLine("    }");

        // Visitor implmentation
        result.AppendLine($"    accept(visitor) {{ return visitor.visit{className}{baseName}(this); }}");

        // End of class
        result.AppendLine("}");
        result.AppendLine();
    }
}
