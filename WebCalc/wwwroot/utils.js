﻿/* SPDX-License-Identifier: ISC */

export class IO {
    /**
     * @callback printCallback
     * @param {...string} messages
     */

    /**
     * @callback errorCallback
     * @param {number} line
     * @param {string} mesasge
     */

    /**
     * @param {?printCallback} onPrint
     * @param {?errorCallback} onError
     */
    constructor(onPrint, onError) {
        this.onPrint = onPrint;
        this.onError = onError;
        this.hadError = false;
    }

    /**
     * Reset the error flag
     */
    reset() {
        this.hadError = false;
    }

    /**
     * Report an error. Calls {@type errorCallback} if it was given to the constructor
     * @param {number} line of input where the error happened
     * @param {string} message description of the error
     */
    error(line, message) {
        this.hadError = true;
        if (typeof this.onError == 'function') {
            this.onError(line, message);
        } else {
            console.warn(`Error on line ${line}: ${message}`);
        }
    }

    /**
     * Report output from the script. Calls {@type printCallback} if one was given to the constructor.
     * @param {...string} messages to print
     */
    print(...messages) {
        if (typeof this.onPrint == 'function') {
            this.onPrint(...messages);
        } else {
            console.log(messages.join(" "));
        }
    }
}

export class Environment {
    /**
     * @param {?Environment} enclosing
     */
    constructor(enclosing) {
        this.values = {};
        this.enclosing = enclosing;
    }

    /**
     * @param {Token} name
     * @param {?any} value
     */
    assign(name, value) {
        if (this.values.hasOwnProperty(name.lexeme)) {
            this.values[name.lexeme] = value;
            return;
        }

        if (this.enclosing != null) {
            enclosing.assign(name, value);
            return;
        }

        throw new RuntimeError(name, `Undefined variable '${name.lexeme}'`);
    }

    /**
     * @param {string} name
     * @param {?any} value
     */
    define(name, value) {
        this.values[name] = value;
    }

    /**
     * @param {Token} name
     * @returns {?any}
     */
    get(name) {
        if (this.values.hasOwnProperty(name.lexeme)) {
            return this.values[name.lexeme];
        }

        if (this.enclosing != null) {
            return this.enclosing.get(name);
        }

        throw new RuntimeError(name, `Undefined variable '${name.lexeme}'`);
    }
}

export class Token {
    /**
     * @param {TokenType} type
     * @param {string} lexeme
     * @param {any?} literal
     * @param {int} line
     */
    constructor(type, lexeme, literal, line) {
        this.type = type;
        this.lexeme = lexeme;
        if (literal != null) {
            this.literal = literal;
        }
        this.line = line;
    }
}

export const TokenType = {
    LEFT_PAREN: "LEFT_PAREN",
    RIGHT_PAREN: "RIGHT_PAREN",
    LEFT_BRACE: "LEFT_BRACE",
    RIGHT_BRACE: "RIGHT_BRACE",
    COMMA: "COMMA",
    DOT: "DOT",
    MINUS: "MINUS",
    PLUS: "PLUS",
    SEMICOLON: "SEMICOLON",
    SLASH: "SLASH",
    STAR: "STAR",

    // One or two character tokens.
    BANG: "BANG",
    BANG_EQUAL: "BANG_EQUAL",
    EQUAL: "EQUAL",
    EQUAL_EQUAL: "EQUAL_EQUAL",
    GREATER: "GREATER",
    GREATER_EQUAL: "GREATER_EQUAL",
    LESS: "LESS",
    LESS_EQUAL: "LESS_EQUAL",

    // Literals.
    IDENTIFIER: "IDENTIFIER",
    STRING: "STRING",
    NUMBER: "NUMBER",

    // Keywords.
    AND: "AND",
    CLASS: "CLASS",
    ELSE: "ELSE",
    FALSE: "FALSE",
    FUN: "FUN",
    FOR: "FOR",
    IF: "IF",
    NIL: "NIL",
    OR: "OR",
    PRINT: "PRINT",
    RETURN: "RETURN",
    SUPER: "SUPER",
    THIS: "THIS",
    TRUE: "TRUE",
    VAR: "VAR",
    WHILE: "WHILE",

    EOF: "EOF"
}

export class RuntimeError extends Error {
    /**
     * @param {Token} token
     * @param {string} message
     */
    constructor(token, message) {
        super(message);
        this.token = token;
    }
}
