/* SPDX-License-Identifier: ISC */
/* File generated at 2022-03-12T19:39:43.3857551+00:00 by Uk.Osric.Caculator.Generator.Program */
export class Assign {
    /**
    * @param {Token} name
    * @param {Expr} value
    */
    constructor(name, value) {
        this.name = name;
        this.value = value;
    }
    accept(visitor) { return visitor.visitAssignExpr(this); }
}

export class Binary {
    /**
    * @param {Expr} left
    * @param {Token} op
    * @param {Expr} right
    */
    constructor(left, op, right) {
        this.left = left;
        this.op = op;
        this.right = right;
    }
    accept(visitor) { return visitor.visitBinaryExpr(this); }
}

export class Grouping {
    /**
    * @param {Expr} expression
    */
    constructor(expression) {
        this.expression = expression;
    }
    accept(visitor) { return visitor.visitGroupingExpr(this); }
}

export class Literal {
    /**
    * @param {?any} value
    */
    constructor(value) {
        this.value = value;
    }
    accept(visitor) { return visitor.visitLiteralExpr(this); }
}

export class Unary {
    /**
    * @param {Token} op
    * @param {Expr} right
    */
    constructor(op, right) {
        this.op = op;
        this.right = right;
    }
    accept(visitor) { return visitor.visitUnaryExpr(this); }
}

export class Variable {
    /**
    * @param {Token} name
    */
    constructor(name) {
        this.name = name;
    }
    accept(visitor) { return visitor.visitVariableExpr(this); }
}

