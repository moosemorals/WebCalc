﻿/* SPDX-License-Identifier: ISC */

import { Interpreter } from './interpreter.js';
import { parse } from './parser.js';
import { scanTokens } from './scanner.js';


/**
 * @param {IO} io
 * @param {string} source
 */
export function run(io, source) {

    const tokens = scanTokens(io, source);
    if (io.hadError) {
        console.log("Scan error");
        return;
    }
    const statements = parse(io, tokens);
    if (io.hadError) {
        console.log("Parse error");
        return;
    }

    const interpreter = new Interpreter(io);
    interpreter.interpret(statements);
    if (io.hadError) {
        console.log("Runtime error");
        return;
    }
}
