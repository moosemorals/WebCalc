﻿/* SPDX-License-Identifier: ISC */

import { Token, TokenType } from './utils.js';

const keywords = {
    "and": "AND",
    "class": "CLASS",
    "else": "ELSE",
    "false": "FALSE",
    "for": "FOR",
    "fun": "FUN",
    "if": "IF",
    "nil": "NIL",
    "or": "OR",
    "print": "PRINT",
    "return": "RETURN",
    "super": "SUPER",
    "this": "THIS",
    "true": "TRUE",
    "var": "VAR",
    "while": "WHILE"
}

let source, start, line, current, tokens;
let io;

/**
 * @param {string} src
 * @returns {Token[]}
 */
export function scanTokens(error, src) {
    io = error;
    source = src;
    start = current = 0;
    line = 1;
    tokens = [];

    while (!isAtEnd()) {
        start = current;
        scanToken();
    }

    tokens.push(new Token(TokenType.EOF, "", null, line));
    return tokens;
}

function scanToken() {
    const c = advance();
    switch (c) {
        case '(': addToken(TokenType.LEFT_PAREN); break;
        case ')': addToken(TokenType.RIGHT_PAREN); break;
        case '{': addToken(TokenType.LEFT_BRACE); break;
        case '}': addToken(TokenType.RIGHT_BRACE); break;
        case ',': addToken(TokenType.COMMA); break;
        case '.': addToken(TokenType.DOT); break;
        case '-': addToken(TokenType.MINUS); break;
        case '+': addToken(TokenType.PLUS); break;
        case ';': addToken(TokenType.SEMICOLON); break;
        case '*': addToken(TokenType.STAR); break;
        case '!':
            addToken(match('=') ? TokenType.BANG_EQUAL : TokenType.BANG);
            break;
        case '=':
            addToken(match('=') ? TokenType.EQUAL_EQUAL : TokenType.EQUAL);
            break;
        case '<':
            addToken(match('=') ? TokenType.LESS_EQUAL : TokenType.LESS);
            break;
        case '>':
            addToken(match('=') ? TokenType.GREATER_EQUAL : TokenType.GREATER);
            break;
        case '/':
            if (match('/')) {
                while (!isAtEnd() && peek() != '\n') {
                    advance();
                }
            } else {
                addToken(TokenType.SLASH);
            }
            break;
        case ' ':
        case '\r':
        case '\t':
            // Ignore whitespace.
            break;

        case '\n':
            line++;
            break;
        case '"': string(); break;
        default:
            if (isDigit(c)) {
                number();
            } else if (isAlpha(c)) {
                identifier();
            } else {
                io.error(line, "Unexpected character");
            }
            break;
    }
}

/**
 * @returns {string}
 */
function advance() { return source[current++] }

/**
 * @param {TokenType} type
 * @param {?string} literal
 */
function addToken(type, literal) {
    const lexeme = source.substring(start, current);
    tokens.push(new Token(type, lexeme, literal, line));
}

function identifier() {
    while (isAlphaNumeric(peek())) {
        advance();
    }

    const text = source.substring(start, current);
    const type = keywords.hasOwnProperty(text) ? keywords[text] : TokenType.IDENTIFIER;
    addToken(type);
}

/**
 * @param {string} c
 * @returns {bool}
 */
function isAlpha(c) {
    return (c >= 'a' && c <= 'z') ||
        (c >= 'A' && c <= 'Z') ||
        (c == '_');
}

/**
 * @param {string} c
 * @returns {bool}
 */
function isAlphaNumeric(c) {
    return isAlpha(c) || isDigit(c);
}

/**
 * @returns {bool}
 */
function isAtEnd() { return current >= source.length }

/**
 * @param {string} c
 * @returns {bool}
 */
function isDigit(c) {
    return c >= '0' && c <= '9';
}

function number() {
    while (isDigit(peek())) {
        advance();
    }
    if (peek() == '.' && isDigit(peekNext())) {
        // consume decimal point
        advance();

        while (isDigit(peek())) {
            advance();
        }

        // Got ourselves a float
        addToken(TokenType.NUMBER, parseFloat(source.substring(start, current)));
        return;
    }
    // Looks like an int
    // This is a meaningless distinction right now, but I've got vague plans
    // to have seperate INTEGER/FLOAT tokens, if not better handling in
    // the interpreter
    addToken(TokenType.NUMBER, parseInt(source.substring(start, current)));
}

/**
 * @param {string} expected
 * @returns {bool}
 */
function match(expected) {
    if (peek() == expected) {
        advance();
        return true;
    }
    return false;
}

/**
 * @returns {string}
 */
function peek() {
    if (isAtEnd()) {
        return '\0';
    }
    return source[current];
}

/**
 * @returns {string}
 */
function peekNext() {
    if (current + 1 >= source.length) {
        return '\0';
    }
    return source[current + 1];
}

function string() {
    while (!isAtEnd() && peek() != '"') {
        if (peek() == '\n') {
            line += 1;
        }
        advance();
    }

    if (isAtEnd()) {
        io.error(line, "Unterminated string");
        return;
    }

    // Consume closing quote
    advance();

    const lexeme = source.substring(start + 1, current - 1);
    addToken(TokenType.STRING, lexeme);
}
