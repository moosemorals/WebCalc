/* SPDX-License-Identifier: ISC */

import { Environment, RuntimeError, Token, TokenType } from './utils.js';
import * as Expr from './expr.js';
import * as Stmt from './stmt.js';

export class Interpreter {
    constructor(IO) {
        this.environment = new Environment();
        this.io = IO;
    }

    /**
     * @param {Stmt[]} statements
     */
    interpret(statements) {
        try {
            for (let i = 0; i < statements.length; i += 1) {
                this.execute(statements[i]);
            }
        } catch (e) {
            if (e instanceof RuntimeError) {
                this.io.error(e.token.line, e.message);
            }
        }
    }

    /**
     * @public
     * @param {Expr.Assign} expr
     * @returns {?any}
     */
    visitAssignExpr(expr) {
        const value = this.evaluate(expr.value);
        this.environment.assign(expr.name, value);
        return value;
    }

    /**
     * @public
     * @param {Stmt.Block} stmt
     */
    visitBlockStmt(stmt) {
        this.executeBlock(stmt.statements, new Environment(this.environment));
    }

    /**
     * @public
     * @param {Expr.Binary} expr
     * @returns {?any}
     */
    visitBinaryExpr(expr) {
        const left = this.evaluate(expr.left);
        const right = this.evaluate(expr.right);

        switch (expr.op.type) {
            case TokenType.BANG_EQUAL:
                return !this.isEqual(left, right);
            case TokenType.EQUAL_EQUAL:
                return this.isEqual(left, right);
            case TokenType.GREATER:
                this.checkNumberOperands(expr.op, left, right);
                return left > right;
            case TokenType.GREATER_EQUAL:
                this.checkNumberOperands(expr.op, left, right);
                return left >= right;
            case TokenType.LESS:
                this.checkNumberOperands(expr.op, left, right);
                return left < right;
            case TokenType.LESS_EQUAL:
                this.checkNumberOperands(expr.op, left, right);
                return left <= right;
            case TokenType.MINUS:
                this.checkNumberOperands(expr.op, left, right);
                return left - right;
            case TokenType.PLUS:
                if (typeof left == 'string' && typeof right == 'string') {
                    return left + right;
                }
                if (typeof left == 'number' && typeof right == 'number') {
                    return left + right;
                }
                throw new RuntimeError(expr.op, "Operands must both be numbers or both strings");
            case TokenType.SLASH:
                return left / right;
            case TokenType.STAR:
                return left * right;
        }
        throw new Error("Unreachable code");
    }

    /**
     * @public
     * @param {Expr.Grouping} expr
     * @returns {?any}
     */
    visitGroupingExpr(expr) {
        return this.evaluate(expr.expression);
    }

    /**
     * @public
     * @param {Stmt.If} stmt     
     */
    visitIfStatement(stmt) {
        if (isTruthy(evaluate(stmt.condition))) {
            execute(stmt.thenBranch);
        } else if (stmt.elseBranch != null) {
            execute(stmt.elseBranch);
        }        
    }

    /**
     * @public
     * @param {Expr.Literal} expr
     * @returns {?any}
     */
    visitLiteralExpr(expr) {
        return expr.value;
    }

    /**
     * @public
     * @param {Stmt.Print} stmt
     */
    visitPrintStmt(stmt) {
        const value = this.evaluate(stmt.expression);
        this.io.print(value);
    }

    /**
     * @public
     * @param {Stmt.Expression} stmt
     */
    visitExpressionStmt(stmt) {
        this.evaluate(stmt.expression);
    }

    /**
     * @public
     * @param {Expr.Unary} expr
     * @returns {?any}
     */
    visitUnaryExpr(expr) {
        const right = this.evaluate(expr.right);
        switch (expr.op.type) {
            case TokenType.MINUS:
                this.checkNumberOperand(expr.op, right);
                return -right;
            case TokenType.BANG:
                return !this.isTruthy(right);
        }
        throw new Error("Unreachable code");
    }

    /**
     * @public
     * @param {Stmt.Var} stmt
     */
    visitVarStmt(stmt) {
        const value = (stmt.initializer != null) ? this.evaluate(stmt.initializer) : null;
        this.environment.define(stmt.name.lexeme, value);
    }

    /**
     * @public
     * @param {Expr.Variable} expr
     * @returns {?any}
     */
    visitVariableExpr(expr) {
        return this.environment.get(expr.name);
    }

    // Support

    /**
     * @param {Token} operator
     * @param {?any} operand
     */
    checkNumberOperand(operator, operand) {
        if (typeof operand == 'number') {
            return;
        }
        throw new RuntimeError(operator, "Operand must be a number");
    }

    /**
     * @param {Token} operator
     * @param {?any} left
     * @param {?any} right
     */
    checkNumberOperands(operator, left, right) {
        if (typeof left == 'number' && typeof right == 'number') {
            return;
        }
        throw new RuntimeError(operator, "Operands must both be numbers");
    }

    /**
     * @private
     * @param {Expr} expr
     * @returns {?any}
     */
    evaluate(expr) {
        return expr.accept(this);
    }

    /**
     * @private
     * @param {Stmt} stmt
     */
    execute(stmt) {
        return stmt.accept(this);
    }

    /**
     * @private
     * @param {Stmt[]} statments
     * @param {Environment} environment
     */
    executeBlock(statements, environment) {
        const previous = this.environment;
        try {
            this.environment = environment;
            for (let i = 0; i < statements.length; i += 1) {
                this.execute(statements[i]);
            }
        } finally {
            this.environment = previous;
        }
    }

    /**
     * @private
     * @param {?any} left
     * @param {?any} right
     * @returns {?any}
     */
    isEqual(left, right) {
        if (left == null && b == null) {
            // nulls are equal to each other
            return true;
        }
        if (left == null) {
            // nulls are different to everything else
            return false;
        }

        // exact equality ("1" != 1)
        return left === right;
    }

    /**
     * @private
     * @param {?any}
     * @returns {bool}
     */
    isTruthy(obj) {
        if (obj == null) {
            return false;
        }
        if (typeof obj == 'boolean') {
            return obj;
        }
        return true;
    }

    /**
     * @param {?any} obj
     * @returns {string}
     */
    stringify(obj) {
        if (obj == null) {
            return "nil";
        }
        return obj.toString();
    }
}
