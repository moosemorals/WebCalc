/* SPDX-License-Identifier: ISC */

import { Token, TokenType } from "./utils.js";
import * as Expr from "./expr.js";
import * as Stmt from "./stmt.js";

let tokens, current;
let io;

/**
 * @param {object} eh
 * @param {Token[]} Tokens
 * @returns {?Stmt}
 */
export function parse(eh, Tokens) {
    io = eh;
    tokens = Tokens;
    current = 0;

    const statements = [];
    while (!isAtEnd()) {
        statements.push(declaration());
    }

    return statements;
}

/**
 * @returns {?Stmt}
 */
function declaration() {
    try {
        if (match(TokenType.VAR)) {
            return varDeclaration();
        }
        return statement();
    } catch (e) {
        console.warn(e);
        io.error(peek().line, e.hasOwnProperty("message") ? e.message : e);
        synchronize();
        return null;
    }
}

/**
 * @returns {Stmt}
 */
function statement() {
    if (match(TokenType.IF)) {
        return ifStatement();
    } else if (match(TokenType.PRINT)) {
        return printStatement();
    } else if (match(TokenType.LEFT_BRACE)) {
        return new Stmt.Block(block());
    } else {
        return expressionStatement();
    }
}

/**
 * @returns {Stmt[]}
 */
function block() {
    const statements = [];

    while (!isAtEnd() && !check(TokenType.RIGHT_BRACE)) {
        statements.push(declaration());
    }

    consume(TokenType.RIGHT_BRACE, "Expected '}' after block");
    return statements;
}

/**
 * @returns {Stmt.Expression}
 */
function expressionStatement() {
    const value = expression();
    consume(TokenType.SEMICOLON, "Expected ';' after expression");
    return new Stmt.Expression(value);
}

/**
 * @returns {Stmt.If}
 */
function ifStatement() {
    consume(TokenType.LEFT_PAREN, "Expected '(' after if.");
    const condition = expression();
    consume(TokenType.RIGHT_PAREN, "Expected ')' after if condition.");

    const thenBranch = statement();
    const elseBranch = match(TokenType.ELSE) ? statement() : null;

    return new Stmt.If(condition, thenBranch, elseBranch);

}

/**
 * @returns {Stmt.Print}
 */
function printStatement() {
    const value = expression();
    consume(TokenType.SEMICOLON, "Expected ';' after value.");
    return new Stmt.Print(value);
}

/**
 * @returns {Stmt.Var}
 */
function varDeclaration() {
    const name = consume(TokenType.IDENTIFIER, "Expected variable name");
    const initiliser = match(TokenType.EQUAL) ? expression() : null;

    consume(TokenType.SEMICOLON, "Expected ';' after variable declaration");
    return new Stmt.Var(name, initiliser);
}

/**
 * @returns {Expr}
 */
function expression() {
    return assignment();
}

/**
 * @returns {Expr}
 */
function assignment() {
    const expr = equality();

    if (match(TokenType.EQUAL)) {
        const equals = previous();
        const value = assignment();

        if (expr instanceof Expr.Variable) {
            const name = expr.name;
            return new Expr.Assign(name, value);
        }

        error(equals, "Invalid assignment target.");
    }

    return expr;
}

/**
 * A function generator for binary operators
 * @param {function(): Expr} next
 * @param {...TokenType} types
 * @returns {function(): Expr}
 */
function generic(next, ...types) {
    return function () {
        let expr = next();

        while (match(...types)) {
            const op = previous();
            const right = next();
            expr = new Expr.Binary(expr, op, right);
        }

        return expr;
    }
}

const factor = generic(unary, TokenType.SLASH, TokenType.STAR);
const term = generic(factor, TokenType.MINUS, TokenType.PLUS);
const comparison = generic(term, TokenType.GREATER, TokenType.GREATER_EQUAL, TokenType.LESS, TokenType.LESS_EQUAL);
const equality = generic(comparison, TokenType.BANG_EQUAL, TokenType.EQUAL_EQUAL);

/**
 * @returns {Expr}
 */
function unary() {
    if (match(TokenType.BANG, TokenType.MINUS)) {
        const op = previous();
        right = unary();
    }
    return primary();
}

/**
 * @returns {Expr}
 */
function primary() {
    if (match(TokenType.FALSE)) {
        return new Expr.Literal(false);
    }
    if (match(TokenType.TRUE)) {
        return new Expr.Literal(true);
    }
    if (match(TokenType.NIL)) {
        return new Expr.Literal(null);
    }

    if (match(TokenType.NUMBER, TokenType.STRING)) {
        return new Expr.Literal(previous().literal);
    }

    if (match(TokenType.IDENTIFIER)) {
        return new Expr.Variable(previous());
    }

    if (match(TokenType.LEFT_PAREN)) {
        const expr = expression();
        consume(TokenType.RIGHT_PAREN, "Expected ')' after expression");
        return new Expr.Grouping(expr);
    }

    throw error(peek(), "Expected expression.");
}

// Support functions
/**
 * @returns {Token}
 */
function advance() {
    if (!isAtEnd()) { current++; }
    return previous();
}

/**
 * @param {TokenType} type
 * @returns {bool}
 */
function check(type) {
    if (isAtEnd()) { return false; }
    return peek().type == type;
}

/**
 * @param {TokenType} type
 * @param {string} message
 * @returns {Token}
 */
function consume(type, message) {
    if (check(type)) {
        return advance();
    }
    throw error(peek(), message);
}

/**
 * @param {Token} token
 * @param {string} message
 * @returns string
 */
function error(token, message) {
    io.error(token.line, message);
    return new ParseError(token.line, message);
}

/**
 * @returns {bool}
 */
function isAtEnd() { return peek().type == TokenType.EOF; }

/***
 * @param {...TokenType} types
 * @returns {bool}
 */
function match(...types) {
    for (let i = 0; i < types.length; i += 1) {
        const type = types[i];
        if (check(type)) {
            advance();
            return true;
        }
    }
    return false;
}

/**
 * @returns {Token}
 */
function peek() { return tokens[current]; }

/**
 * @returns {Token}
 */
function previous() { return tokens[current - 1]; }

function synchronize() {
    advance();

    while (!isAtEnd()) {
        if (previous().type == TokenType.SEMICOLON) {
            return;
        }

        switch (peek().type) {
            case TokenType.CLASS:
            case TokenType.FUN:
            case TokenType.VAR:
            case TokenType.FOR:
            case TokenType.IF:
            case TokenType.WHILE:
            case TokenType.PRINT:
            case TokenType.RETURN:
                return;
        }
        advance();
    }
}

class ParseError extends Error {
    constructor(line, message) {
        super(message);
        this.line = line;
    }
}
