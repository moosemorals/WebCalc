/* SPDX-License-Identifier: ISC */
/* File generated at 2022-03-12T19:39:43.3941848+00:00 by Uk.Osric.Caculator.Generator.Program */
export class Block {
    /**
    * @param {Stmt[]} statements
    */
    constructor(statements) {
        this.statements = statements;
    }
    accept(visitor) { return visitor.visitBlockStmt(this); }
}

export class Expression {
    /**
    * @param {Expr} expression
    */
    constructor(expression) {
        this.expression = expression;
    }
    accept(visitor) { return visitor.visitExpressionStmt(this); }
}

export class Print {
    /**
    * @param {Expr} expression
    */
    constructor(expression) {
        this.expression = expression;
    }
    accept(visitor) { return visitor.visitPrintStmt(this); }
}

export class Var {
    /**
    * @param {Token} name
    * @param {?Expr} initializer
    */
    constructor(name, initializer) {
        this.name = name;
        this.initializer = initializer;
    }
    accept(visitor) { return visitor.visitVarStmt(this); }
}

