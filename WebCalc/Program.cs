using System;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

WebApplicationBuilder? builder = WebApplication.CreateBuilder(args);

WebApplication? app = builder.Build();

DefaultFilesOptions defaultFilesOptions = new();
defaultFilesOptions.DefaultFileNames.Clear();
defaultFilesOptions.DefaultFileNames.Add("index.html");
app.UseDefaultFiles(defaultFilesOptions);

app.UseStaticFiles(new StaticFileOptions {
    OnPrepareResponse = ctx => {
        IHeaderDictionary headers = ctx.Context.Response.Headers;
        // all
        headers.Add("cache-control", "public, max-age=300");
        headers.Add("X-Content-Type-Options", "nosniff");

        // html only
        if (ctx.File.Name.EndsWith(".html", StringComparison.InvariantCulture)) {
            headers.Add("X-Frame-Options", "deny");
            headers.Add("Content-Security-Policy", "default-src 'self' 'unsafe-inline'");
        }
    }
});

app.Run();
