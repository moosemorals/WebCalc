
# WebCalc

Using [Lox](https://www.craftinginterpreters.com/) to build a web based
desktop calcuator.

Will be adding PWA stuff 'soon' so it can be installed.

# Licence

This project is distributed under the terms of the [ISC Licence](LICENCE.txt). 

# Warning!

Verify all calcuated results manually.